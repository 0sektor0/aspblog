using System;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;



namespace aspblog.Models
{
    public class User : IdentityUser
    {
        public DateTime CreationDate { get; set; } = DateTime.UtcNow;
    }
}